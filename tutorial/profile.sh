#!/bin/bash

# We want to add our custom sections but also retain the default
# ones that ship with the tool. We can do this by locating the
# default sections directory, which is either in the CUDA toolkit
# nsight-compute-20xx.yy directory or at the same level as the ncu
# binary, depending on where it was installed.

ncu_dir=$(dirname $(which ncu))
ncu_version=$(ncu --version | grep Version | awk '{ print $2 }')

if [ -d $ncu_dir/sections ]; then
    root_sections_dir=$ncu_dir/sections
elif [ -d $ncu_dir/../nsight-compute-$ncu_version/sections ]; then
    root_sections_dir=$ncu_dir/../nsight-compute-$ncu_version/sections
elif [ -d /opt/nvidia/nsight-compute/$ncu_version/sections ]; then
    root_sections_dir=/opt/nvidia/nsight-compute/$ncu_version/sections
else
    echo "Nsight Compute installation not found"
    exit
fi

ncu -f -o $1 --section-folder ncu-sections --section-folder $root_sections_dir --set full ./gpp.x
