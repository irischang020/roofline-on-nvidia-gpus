# Roofline Analysis Tutorial

## Some Toy Examples

Before we analyze the real GPP example, let's play with some toy kernels to warm up using Nsight Compute to do roofline analysis.
The code tutorial.cu is a simple CUDA C++ code that contains three kernels, kernel_A, kernel_B, and kernel_C. Compile the code and
profile it with Nsight Compute, then load the report into the Nsight Compute UI:

```
nvcc -o tutorial tutorial.cu
ncu -f -o tutorial --set full ./tutorial
```

kernel_A is an instruction throughput bound kernel: with each thread we do 10000 double precision adds, for an arithmetic intensity of
10000 / 8 (bytes per double precision word) = 1250. Clearly this is a contrived example, but our goal here is to examine whether the
tool supports our intuition that this should be in the compute-bound part of the roofline chart. Does Nsight Compute agree with that?

kernel_B is identical to kernel_A, except that we artificially throttle occupancy on the GPU by allocating 96 kB of shared memory per
threadblock, which means that only one threadblock can be resident on an SM at any one time, for an occupancy of 1/32 = 3.125%. This
kernel will likely be instruction latency bound -- it should have the same arithmetic intensity as kernel_A, but not be anywhere close
to the compute-bound roofline.

kernel_C is primarily memory-bandwidth bound -- we just do a single double precision add, combined with a load and a store (for an
arithmetic intensity of 1 / 16 = .0625). The memory access pattern is strided -- we load every element of B exactly once and store
every element of A exactly once, but any given warp is accessing memory locations 32 bytes apart between each thread. (Can you determine
which part of the memory subsystem ought to be the bottleneck for this kernel?)

Take some time to examine the Nsight Compute report for each kernel. Pay attention to not just the roofline chart but the rest of
the report as well: for all cases, does the information in the compute workload analysis, memory workload analysis, and instruction
statistics sections match your understanding of what should be going on in each kernel?

## GPP Exercise

We provide four git patches that demonstrate a series of optimizations applied to the GPP kernel to improve its performance.
These are described below. A workflow we recommend is to first profile the baseline code, and then apply each patch
consecutively, collecting a new profile each time. You can load multiple report files into Nsight Compute and, if you like,
use the "Add Baseline" functionality to see multiple reports on the same roofline chart so you can see the progress made
in each step.

The git patches are cumulative, so the intended workflow is something like:

```
cd ..
make
./profile.sh baseline

git apply tutorial/step1.patch
make
./profile.sh step1

git checkout .
git apply tutorial/step2.patch
make
./profile.sh step2
```

and so on.

## Description of optimization steps

### Step 1

In the baseline code, roofline analysis reveals that this kernel is not compute-bound. However its arithmetic intensity
is just below the crossover point at which the kernel can be said to be compute bound. This suggests that if we do a little
work, we can get the application over to the compute bound regime, where we may be more likely to achieve a higher fraction
of peak performance. (Contrast this with a simple kernel that, say, copies one array to another -- there is no way to rewrite
this to move it from the bandwidth-bound regime to the compute-bound regime.)

Another consideration is that with any two of the three loops that are parallelized in the baseline code, we have enough
parallelism to saturate a modern GPU (which can have O(100,000) threads resident). So this is a case where we can inspect
a different parallelism strategy on at least one of the three loops without worrying that the resulting kernel will have
low occupancy on the GPU.

Combining these two strands of thought together, we could choose to collapse only two of the three loops, leaving the third
loop to be run sequentially by each thread. Since each thread would have more work, it is possible that the kernel could become
compute bound.

To choose which one to try, we should pay attention to the memory access patterns of the code. Fortran uses column major
memory ordering, so 2D arrays like wtilde_array and I_eps_array have locations indexed with ig adjacent to each other in
memory. Similarly, arrays like aqsmtemp_local have locations indexed with igp adjacent to each other in memory. So for all
of the multi-dimensional arrays the n1_loc has the largest stride between accesses. Effective use of GPU memory bandwidth
requires coalesced accesses where consecutive threads access consecutive locations in memory. So this all implies that the
n1_loc loop is the most logical target for this experiment.

When we try this, we find that the code may not actually speed up. However, we find that we have now definitely made the
kernel compute bound, with a double precision arithmetic intensity of around 20 flops / byte. This means that if we can make
the computation more efficient we might be able to get closer to the peak.

### Step 2

The GPP kernel has a reduction on the arrays `ssx_array` and `sch_array` (which are length 3). The baseline code has already
unrolled this loop and done the reduction over six scalars (which makes sense because at this time OpenACC does not implement
array reductions). However, this means that the kernel is doing reductions on three variables, increasing memory traffic and
involving operations like threadblock synchronizations. In this step 2 we move the innermost loop outside the kernel, so that
we run the kernel three consecutive times, reducing on the relevant variable each time. This results in a modest increase in
performance, implying that the runtime of each of the three launched kernels is less than 1/3 of the original runtime. The
arithmetic intensity decreases a bit, though the kernel is still compute bound.

### Step 3

There are a couple of points in the code that involve a double precision divide of two complex numbers. A division in complex
arithmetic is a little more involved than a division in real numbers: if `z1 = a + bi` and `z2 = c + di`, then `z1 / z2 = (ac + bd) / K +
((bc - ad) / K) i`, where `K = c^2 + d^2`; but still, we ultimately end up with a floating point divide, and if we inspect the PTX
instructions emitted for this kernel (say, with `cuobjdump -ptx ./gpp.x`) we will see `div.rn.f64` instructions emitted. Floating point
divisions are generally slow and require many cycles compared to multiplies/adds.

However, NVIDIA GPUs also have a faster reciprocal operation for double precision numbers (`rcp.rn.f64`), so we coax the compiler into
taking this path by explicitly constructing the reciprocal of the complex numbers first, and then adjusting the calculation appropriately
to use this reciprocal. Inspection of the PTX verifies that this has occurred.

Another quirk of complex math is that while the absolute value of a real number is a simple operation, the absolute value of a complex number
is given by `a + bi = sqrt(a^2 + b^2)`. This step replaces the explicit calculation of these absolute value operations, noticing that we don't
actually need them explicitly, we just need the final comparison of ssx to ssxcutoff to be valid.

This step results in a significant (~40%) speedup. Looking at the roofline chart, we have decreased our arithmetic intensity by quite a bit,
returning to the bandwidth bound part of the chart; however, we are also a bit closer to the roofline than when we started in the baseline.

### Step 4

At this point we have probably picked most of the low-hanging fruit. It is at this point that we can consider tuning how the parallelism
maps to the hardware (this would have been premature during an earlier step -- it's usually best to save this step for last, so that you
do not lock yourself into a particular parallel pattern that can hide other opportunities for a speedup). In OpenACC the simplest way to
do this is by controlling the vector_length parameter, which translates in CUDA to the number of threads per block. In most applications
running on NVIDIA GPUs, the best choice is empirically determined. It turns out for this kernel that 512 threads per block is a good choice.
Inspection of the Nsight Compute report shows that this is actually mostly the result of the fact that this threadblock size requires the
compiler to only use 128 registers per thread, and the increase in occupancy that results from fitting more threads per SM at once (due to
the decreased register load) evidently more than compensates from whatever inefficiencies come from using fewer registers (if any). We could
also have achieved this same effect by directly capping the register count (for the PGI compiler, use -ta=tesla:maxregcount:128).

## Next steps

At this point you should experiment with some other possible optimizations to this kernel and practice collecting the report and analyzing
it in Nsight Compute. Two potential optimizations that we recommend trying are (1) move redundant computations (ones that don't depend on
n1_loc) outside of the innermost loop and (2) cache repeated array accesses to the same index in temporary variables. Do these help? If so,
that suggests the compiler couldn't figure these optimizations out for you -- is that surprising? There are other possibilities too, such as
further modifying the kernel to use multiple levels of parallelism rather than collapsing the loops, so give it your best shot. It's OK if
you can't make the code any faster -- what matters is gaining experience thinking about performance analysis on GPUs.
