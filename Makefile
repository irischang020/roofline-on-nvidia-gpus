
gpp.x: gpp.f90 gpp_data.f90
	pgfortran -acc -ta=tesla -Mcuda=cuda10.1 -Minfo=accel -fast -Mfree -mp -Mlarge_arrays gpp_data.f90 gpp.f90 -o gpp.x

clean:
	rm -f *.o *.mod gpp.x
