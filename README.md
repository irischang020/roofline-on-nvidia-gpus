# Roofline Model on NVIDIA GPUs

This repo demonstrates the use of Roofline analysis on NVIDIA GPUs especially on V100s and architectures after V100s. 
The Roofline performance model provides an intuitive and insightful way to understand application performance, identify bottlenecks and perform optimization for HPC applications. 
For more details on Roofline, please visit [this page](https://crd.lbl.gov/departments/computer-science/PAR/research/roofline/).

The methodology for Roofline data collection on NVIDIA GPUs has evolved from using nvprof ([github:nersc-roofline](https://github.com/cyanguwa/nersc-roofline)), to Nsight Compute in CUDA 10 ([tag:cuda10.2.89-ncu](https://gitlab.com/NERSC/roofline-on-nvidia-gpus/-/tags/cuda10.2.89-ncu)) to Nsight Compute in CUDA 11 ([tag:cuda11.0.167-ncu](https://gitlab.com/NERSC/roofline-on-nvidia-gpus/-/tags/cuda11.0.167-ncu)).
A few papers have been published to validate the efficacy of the methodology:
 
- C. Yang, T. Kurth, and S. Williams, Hierarchical Roofline analysis for GPUs: Accelerating performance optimization for the NERSC‐9 Perlmutter system, Concurrency and Computation: Practice and Experience, e5547, 2019. https://doi.org/10.1002/cpe.5547
- Y. Wang, C. Yang, Y. Zhang, T. Kurth and S. Williams, Roofline Performance Analysis of Operator Fusion in Deep Learning Applications, Cray User Group (CUG), 2020. (accepted)
- C. Yang, S. Williams, and Y. Wang, Roofline Performance Model for HPC and Deep-Learning Applications, GPU Technology Conference (GTC), 2020. https://developer.nvidia.com/gtc/2020/video/s21565


The application in question in this repo is the GPP (General Plasmon Pole) kernel from the Sigma module in [BerkeleyGW](https://berkeleygw.org). 
This is a mini-app that abstracts a key part of the self-energy calculation in GW workflows in Material Science. 


## Workflow of Roofline Data Collection

The structure of this repo is as follows.

- A baseline version of the GPP code is provided, as well as an example input file and a makefile.
  + `gpp.f90` and `gpp_data.f90`
  + `gpp214unformatted.dat`
  + `Makefile`  

  The code is accelerated with OpenACC using the PGI compiler. 
This baseline code can be thought of as the naive GPU port of the original CPU code, and we simply collapse the three available independent loops to expose maximum parallelism.
Four other versions are available by applying the git patches in [/tutorial](/tutorial). 
These patches demonstrate four optimization steps to improve the kernel performance by 2.5x, and can be applied to the baseline code directly. 
Please see [/tutorial/README.md](/turorial/README.md) for more details.

- To profile the code with Nsight Compute, an example job script and several section files are provided.
  + `run.nsight.compute`
  + `/ncu-sections/SpeedOfLight.section`
  + `/ncu-sections/SpeedOfLight_RooflineChart.section`
  + `/ncu-sections/SpeedOfLight_HierarchicalDoubleRooflineChart.section`  

  The `run.nsight.compute` script runs the code and produces an Nsight Compute profile `.ncu-rep`, which can be then opened with the Nsight Compute GUI, either on Cori via `nv-nsight-cu` or using your local laptop version of the Nsight Compute viewer.
These section files are respectively for,
the Speed of Light section, the Speed of Light (HBM-only) Roofline Chart, and the Speed of Light double-precision hierarchical Roofline Chart. 
The first two are shipped with Nsight Compute 2020.1, and the third one is a custom section file, covering not only device memory level Roofline, but also L2 and L1 levels.

  This custom section file can be extended to single-precision, half-precision, and for Tensor Core operations. Please try the following files for these operations:
  + `/ncu-sections/SpeedOfLight_HierarchicalSingleRooflineChart.section`
  + `/ncu-sections/SpeedOfLight_HierarchicalHalfRooflineChart.section` 
  + `/ncu-sections/SpeedOfLight_HierarchicalTensorRooflineChart.section`  


- For users who would like to integrate the Roofline data collection into their own workflow, a set of scripts are provided for collecting relevant metrics manually and plotting Roofline charts on their own.
  + `run.customized`
  + `postprocess.py` and `roofline.py`

  The `run.customized` script collects the necessary metrics using the command-line utility of Nsight Compute `nv-nsight-cu-cli` and produces a `.csv` file. 
This file is then processed by `postprocess.py`, a Pandas-based Python script, to calculate the Arithmetic Intensity (AI) and FLOP/s throughput for each kernel profiled.
The `postprocess.py` script will then call `roofline.py` to plot Roofline charts using Matplotlib and save images in `.png` files. 

  **These scripts are written specifically for GPP and the file structure in this repo, so please modify the location of files, names of output files, etc to suit your own needs!**  

  **The methodology used in these scripts is new from Nsight Compute in CUDA 11. Please give it a try and let us know if there is any problem.**

  - `Time`:  
    + sm__cycles_elapsed.avg / sm__cycles_elapsed.avg.per_second
  - `FLOPs`:  
    + `DP`: sm__sass_thread_inst_executed_op_dadd_pred_on.sum + 2 x sm__sass_thread_inst_executed_op_dfma_pred_on.sum + sm__sass_thread_inst_executed_op_dmul_pred_on.sum  
    + `SP`: sm__sass_thread_inst_executed_op_fadd_pred_on.sum + 2 x sm__sass_thread_inst_executed_op_ffma_pred_on.sum + sm__sass_thread_inst_executed_op_fmul_pred_on.sum  
    + `HP`: sm__sass_thread_inst_executed_op_hadd_pred_on.sum + 2 x sm__sass_thread_inst_executed_op_hfma_pred_on.sum + sm__sass_thread_inst_executed_op_hmul_pred_on.sum  
    + `Tensor Core`: 512 x sm__inst_executed_pipe_tensor.sum
  - `Bytes`:  
    + `DRAM`: dram__bytes.sum  
    + `L2`: lts__t_bytes.sum  
    + `L1`: l1tex__t_bytes.sum
